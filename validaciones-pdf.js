
/* Validaciaones del COAATM Dossier de colegiación */

function Validar() {
    // validaciones javascript al Guardar

    firmaDigital = this.getField('Firma digital');
    firmaInput = this.getField('Firma digital deshabilitada');
    ErroresValidacion = 0;
    mensajeValidacionFinal = "";

    firmaDigital.hidden = false;
    firmaInput.hidden = true;

    //Nombre
    var nombre = this.getField('TXT_NOMBRE').value;

    if (nombre == "") {
        //this.getField('TXT_NOMBRE').setFocus();
        //app.alert('Por favor introduce un valor en el campo Nombre');
        mensajeValidacionFinal += 'Por favor introduce un valor en el campo Nombre\n';
        ErroresValidacion++;
        this.getField('TXT_NOMBRE_READ').value = nombre;
    } else {
        this.getField('TXT_NOMBRE_READ').value = nombre;
    }

    //Apellido 1
    var Apellido1 = this.getField('TXT_APELLIDO1').value;

    if (Apellido1 == "") {
        //this.getField('TXT_APELLIDO1').setFocus();
        //app.alert('Por favor introduce un valor en el campo 1er APELLIDO');
        mensajeValidacionFinal += 'Por favor introduce un valor en el campo 1er APELLIDO\n';
        ErroresValidacion++;
        this.getField('TXT_APELLIDO1_READ').value = Apellido1;
    } else {
        this.getField('TXT_APELLIDO1_READ').value = Apellido1;
    }

    //Apellido 2
    var Apellido2 = this.getField('TXT_APELLIDO2').value;

    if (Apellido2 == "") {
        //this.getField('TXT_APELLIDO2').setFocus();
        //app.alert('Por favor introduce un valor en el campo 2º APELLIDO');
        mensajeValidacionFinal += 'Por favor introduce un valor en el campo 2º APELLIDO\n';
        ErroresValidacion++;
        this.getField('TXT_APELLIDO2_READ').value = Apellido2;
    } else {
        this.getField('TXT_APELLIDO2_READ').value = Apellido2;
    }

    //DNI
    // Acepta NIEs (Extranjeros con X, Y o Z al principio)
    var dni = this.getField('TXT_NIF').value;
    var numero
    var letraDni
    var letra
    var expresion_regular_dni

    expresion_regular_dni = /^\d{8}[a-zA-Z]$/;

    // comprobacion de dni vacio
    if (dni == "") {
        //this.getField('TXT_NIF').setFocus();
        //app.alert('Por favor introduce un valor en el campo NIF');
        mensajeValidacionFinal += 'Por favor introduce un valor en el campo NIF\n';
        ErroresValidacion++;
    } else {
    }
    // comprobación del dni
    if (expresion_regular_dni.test(dni) == true) {
        numero = dni.substr(0, dni.length - 1);
        letraDni = dni.substr(dni.length - 1, 1);
        numero = numero % 23;
        letra = 'TRWAGMYFPDXBNJZSQVHLCKET';
        letra = letra.substring(numero, numero + 1);
        if (letra != letraDni.toUpperCase()) {
            //this.getField('TXT_NIF').setFocus();
            //app.alert('Dni erroneo, la letra del NIF no se corresponde');
            mensajeValidacionFinal += 'Dni erroneo, la letra del NIF no se corresponde\n';
            ErroresValidacion++;
        } else {
            //      app.alert('Dni correcto');
        }
    } else {
        //this.getField('TXT_NIF').setFocus();
        //app.alert('Dni erroneo, formato no válido');
        mensajeValidacionFinal += 'Dni erroneo, formato no válido\n';
        ErroresValidacion++;
    }

    //fecha alta
    // validaciones de fecha
    fechaAlta = this.getField('FEC_ALTA').value;

    var expresion_regular_fecha = /^([0][1-9]|[12][0-9]|3[01])(\/|-)([0][1-9]|[1][0-2])\2(\d{4})$/;
    if ((expresion_regular_fecha.test(fechaAlta) == true) && (fechaAlta != '')) {
        // es una fecha valida
    } else {
        //this.getField('FEC_ALTA').setFocus();
        //app.alert('El campo Fecha de alta esta vacio o no tiene el formato correcto.');
        mensajeValidacionFinal += 'El campo Fecha de alta esta vacio o no tiene el formato correcto.\n';
        ErroresValidacion++;
    }

    //Fecha Nacimiento
    fechaNacimiento = this.getField('FEC_NACIMIENTO').value;

    var expresion_regular_fecha = /^([0][1-9]|[12][0-9]|3[01])(\/|-)([0][1-9]|[1][0-2])\2(\d{4})$/;
    if ((expresion_regular_fecha.test(fechaNacimiento) == true) && (fechaNacimiento != '')) {
        // es una fecha valida
    } else {
        //this.getField('FEC_NACIMIENTO').setFocus();
        //app.alert('El campo Fecha de nacimiento esta vacio o no tiene el formato correcto.');
        mensajeValidacionFinal += 'El campo Fecha de nacimiento esta vacio o no tiene el formato correcto.\n';
        ErroresValidacion++;
    }

    //lugar de nacimiento
    var lugarNacimiento = this.getField('TXT_LUGAR_NACIMIENTO').value;

    if (lugarNacimiento == "") {
        //this.getField('TXT_LUGAR_NACIMIENTO').setFocus();
        //app.alert('Por favor introduce un valor en el campo Lugar de nacimiento');
        mensajeValidacionFinal += 'Por favor introduce un valor en el campo Lugar de nacimiento\n';
        ErroresValidacion++;
    } else {
    }

    //provincia nacimiento
    var provinciaNacimiento = this.getField('TXT_PROVINCIA_NACIMIENTO').value;

    if (provinciaNacimiento == "") {
        //this.getField('TXT_PROVINCIA_NACIMIENTO').setFocus()
        //app.alert('Por favor introduce un valor en el campo Provincia');
        mensajeValidacionFinal += 'Por favor introduce un valor en el campo Provincia\n';
        ErroresValidacion++;
    } else {
    }

    //pais nacimiento
    var paisNacimiento = this.getField('TXT_PAIS_NACIMIENTO').value;
    if (paisNacimiento == "") {
        //this.getField('TXT_PAIS_NACIMIENTO').setFocus();
        //app.alert('Por favor introduce un valor en el campo País');
        mensajeValidacionFinal += 'Por favor introduce un valor en el campo País\n';
        ErroresValidacion++;
    } else {
    }

    //estado civil
    var estadoCivil = this.getField('OPT_EC').value;
    if (estadoCivil == "Off") {
        //this.getField('OPT_EC').setFocus();
        //app.alert('Debe seleccionar una opción para el campo Estado Civil');
        mensajeValidacionFinal += 'Debe seleccionar una opción para el campo Estado Civil\n';
        ErroresValidacion++;
    } else {
    }

    //valor del campo radio
    //app.alert('el valor del campo Estado civil es ' + estadoCivil);

    // Sexo
    var sexo = this.getField('OPT_S').value;
    if (sexo == "Off") {
        //this.getField('OPT_S').setFocus();
        //app.alert('Debe seleccionar una opción para el campo Sexo');
        mensajeValidacionFinal += 'Debe seleccionar una opción para el campo Sexo\n';
        ErroresValidacion++;
    } else {
    }

    //valor del campo radio
    //app.alert('el valor del campo Estado civil es ' + sexo);

    // fecha fin de carrera
    fechaFinCarrera = this.getField('FEC_FINCARRERA').value;

    var expresion_regular_fecha = /^([0][1-9]|[12][0-9]|3[01])(\/|-)([0][1-9]|[1][0-2])\2(\d{4})$/;
    if ((expresion_regular_fecha.test(fechaFinCarrera) == true) && (fechaFinCarrera != '')) {
        // es una fecha valida
    } else {
        //this.getField('FEC_FINCARRERA').setFocus();
        //app.alert('El campo Fecha fin de carrera esta vacio o no tiene el formato correcto.');
        mensajeValidacionFinal += 'El campo Fecha fin de carrera esta vacio o no tiene el formato correcto.\n';
        ErroresValidacion++;
    }

    // fecha expedición título
    fechaExpTitulo = this.getField('FEC_EXPEDICIONTITULO').value;

    var expresion_regular_fecha = /^([0][1-9]|[12][0-9]|3[01])(\/|-)([0][1-9]|[1][0-2])\2(\d{4})$/;
    if ((expresion_regular_fecha.test(fechaExpTitulo) == true) || (fechaExpTitulo == '')) {
        // es una fecha valida
    } else {
        //this.getField('FEC_FINCARRERA').setFocus();
        //app.alert('El campo Fecha de expedición del título no tiene el formato correcto.');
        mensajeValidacionFinal += 'El campo Fecha de expedición del título no tiene el formato correcto.\n';
        ErroresValidacion++;
    }


    // via1
    var via1 = this.getField('CBO_DP_TPVIA').value;
    if (via1 == "0") {
        //this.getField('CBO_DP_TPVIA').setFocus();
        //app.alert('Debe seleccionar una opcion para Vía');
        mensajeValidacionFinal += 'Debe seleccionar una opcion para Vía\n';
        ErroresValidacion++;
    } else {
    }

    //valor combo
    //app.alert(via1);

    //Desc via1
    var descVia1 = this.getField('TXT_DP_VIA').value;
    if (descVia1 == "") {
        //this.getField('TXT_DP_VIA').setFocus();
        //app.alert('Por favor introduce un valor en el campo Desc Vía');
        mensajeValidacionFinal += 'Por favor introduce un valor en el campo Desc Vía\n';
        ErroresValidacion++;
    } else {
    }

    //valor combo
    //app.alert(descVia1);

    var provincia1 = this.getField('CBO_DP_PROVINCIA').value;
    if (provincia1 == "66") {
        //this.getField('CBO_DP_PROVINCIA').setFocus();
        //app.alert('Debe seleccionar una opcion para Provincia');
        mensajeValidacionFinal += 'Debe seleccionar una opcion para Provincia\n';
        ErroresValidacion++;
    } else {
    }

    //valor combo
    //app.alert(provincia1);

    //codigo postal1
    var codigoPostal1 = this.getField('TXT_DP_CODPOSTAL').value;
    if (codigoPostal1 == "") {
        //this.getField('TXT_DP_CODPOSTAL').setFocus();
        //app.alert('Por favor introduce un valor en el campo Código Postal');
        mensajeValidacionFinal += 'Por favor introduce un valor en el campo Código Postal\n';
        ErroresValidacion++;
    } else {
    }

    //email1

    correo1 = this.getField('TXT_DP_EMAIL').value;

    var expresion_regular_correo = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
    if ((expresion_regular_correo.test(correo1) == true)) {
        // es una fecha valida
    } else {
        //this.getField('FEC_FINCARRERA').setFocus();
        //app.alert('El campo email tiene un formato incorrecto.');
        mensajeValidacionFinal += 'El campo email tiene un formato incorrecto.\n';
    }

    //valor combo
    //app.alert(codigoPostal1);

    // domicilio de contacto
    var via2 = this.getField('CBO_DC_TPVIA').value;
    var descVia2 = this.getField('TXT_DC_VIA').value;
    var provincia2 = this.getField('CBO_DC_PROVINCIA').value;
    var codigoPostal2 = this.getField('TXT_DC_CODPOSTAL').value;

    if (via2 != "0" || descVia2 != "" || provincia2 != "66" || codigoPostal2 != "") {
        //app.alert('Si rellena la sección de Domicilio de contacto los campos Vía, Desc Vía, Provincia, y codigo postal son obligatorios');
        //mensajeValidacionFinal += 'Si rellena la sección de Domicilio de contacto los campos Vía, Desc Vía, Provincia, y codigo postal son obligatorios\n';
        //ErroresValidacion++;
        if (via2 == "0") {
            //app.alert('Debe seleccionar una opcion para Vía en domicilio de contacto');
            mensajeValidacionFinal += 'Debe seleccionar una opcion para Vía en domicilio de contacto\n';
            ErroresValidacion++;
        } else {
        }
        if (descVia2 == "") {
            //app.alert('Por favor introduce un valor en el campo Desc Vía en domicilio de contacto');
            mensajeValidacionFinal += 'Por favor introduce un valor en el campo Desc Vía en domicilio de contacto\n';
            ErroresValidacion++;
        } else {
        }
        if (provincia2 == "66") {
            //app.alert('Debe seleccionar una opcion para Provincia en domicilio de contacto');
            mensajeValidacionFinal += 'Debe seleccionar una opcion para Provincia en domicilio de contacto\n';
            ErroresValidacion++;
        } else {
        }
        if (codigoPostal2 == "") {
            //app.alert('Por favor introduce un valor en el campo Código Postal en domicilio de contacto');
            mensajeValidacionFinal += 'Por favor introduce un valor en el campo Código Postal en domicilio de contacto\n';
            ErroresValidacion++;
        } else {
        }

        //email1

        correo2 = this.getField('TXT_DP_EMAIL').value;

        var expresion_regular_correo = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
        if ((expresion_regular_correo.test(correo2) == true)) {
            // es una fecha valida
        } else {
            //this.getField('FEC_FINCARRERA').setFocus();
            //app.alert('El campo email de domicilio de contacto tiene un formato incorrecto.');
            mensajeValidacionFinal += 'El campo email de domicilio de contacto tiene un formato incorrecto.\n';
            //firmaDigital.hidden = true;
            //firmaInput.hidden = false;
            //no meter contador
        }
    } else {

    }


    //validaciones cuenta corriente1

    // Validacion del numero de cuenta 1
    var iban1 = this.getField('TXT_COD_IBAN_1').value.toString();
    var banco1 = this.getField('TXT_BANCO_1').value.toString();
    var agencia1 = this.getField('TXT_AGENCIA_1').value.toString();
    var dc1 = this.getField('TXT_DC_1').value.toString();
    var nCuenta1 = this.getField('TXT_NUMCUENTA_1').value.toString();

    // Funcionalidad para que no elimine los 0
    while (banco1.length < 4) {
        banco1 = '0' + banco1;
    }
    while (agencia1.length < 4) {
        agencia1 = '0' + agencia1;
    }
    while (dc1.length < 2) {
        dc1 = '0' + dc1;
    }
    while (nCuenta1.length < 10) {
        nCuenta1 = '0' + nCuenta1;
    }

    numeroCuenta1 = banco1 + agencia1 + dc1 + nCuenta1;
    ibanconCuenta = iban1 + banco1 + agencia1 + dc1 + nCuenta1; //cuenta con iban
    //app.alert(banco1);
    //app.alert(agencia1);
    //app.alert(dc1);
    //app.alert(nCuenta1);
    //app.alert(numeroCuenta1);
    //app.alert(dc1);
    //app.alert(nCuenta1);

    valores = new Array(1, 2, 4, 8, 5, 10, 9, 7, 3, 6);
    controlCS = 0;
    controlCC = 0;
    for (i = 0; i <= 7; i++)
        controlCS += parseInt(numeroCuenta1.charAt(i)) * valores[i + 2];
    controlCS = 11 - (controlCS % 11);
    if (controlCS == 11) controlCS = 0;
    else if (controlCS == 10) controlCS = 1;

    for (i = 10; i <= 19; i++)
        controlCC += parseInt(numeroCuenta1.charAt(i)) * valores[i - 10];
    controlCC = 11 - (controlCC % 11);
    if (controlCC == 11) controlCC = 0;
    else if (controlCC == 10) controlCC = 1;

    if (numeroCuenta1.charAt(8) == controlCS && numeroCuenta1.charAt(9) == controlCC) {
        //app.alert('numero de cuenta valido');
        //app.alert(numeroCuenta1);
    } else {
        //app.alert('El primer numero de cuenta es invalido o faltan digitos, reviselo');
        mensajeValidacionFinal += 'El primer numero de cuenta es invalido o faltan digitos, reviselo\n';
        ErroresValidacion++;
        //app.alert(numeroCuenta1);
    }

    /* Validar el iban de la cuenta bancaria */
    // Funcionalidad para que no elimine los 0
    // Validacion del numero de cuenta 1
    var iban1 = this.getField('TXT_COD_IBAN_1').value.toString();
    var banco1 = this.getField('TXT_BANCO_1').value.toString();
    var agencia1 = this.getField('TXT_AGENCIA_1').value.toString();
    var dc1 = this.getField('TXT_DC_1').value.toString();
    var nCuenta1 = this.getField('TXT_NUMCUENTA_1').value.toString();

    // Funcionalidad para que no elimine los 0
    while (banco1.length < 4) {
        banco1 = '0' + banco1;
    }
    while (agencia1.length < 4) {
        agencia1 = '0' + agencia1;
    }
    while (dc1.length < 2) {
        dc1 = '0' + dc1;
    }
    while (nCuenta1.length < 10) {
        nCuenta1 = '0' + nCuenta1;
    }

    numeroCuenta1 = banco1 + agencia1 + dc1 + nCuenta1;
    IBAN = iban1 + banco1 + agencia1 + dc1 + nCuenta1; //cuenta con iban

    function fn_ValidateIBAN(IBAN) {

        //Se pasa a Mayusculas
        IBAN = IBAN.toUpperCase();
        //Se quita los blancos de principio y final.
        //IBAN = IBAN.trim();
        IBAN = IBAN.replace(/\s/g, ""); //Y se quita los espacios en blanco dentro de la cadena

        var letra1, letra2, num1, num2;
        var isbanaux;
        var numeroSustitucion;
        //La longitud debe ser siempre de 24 caracteres
        if (IBAN.length != 24) {
            //app.alert('Primera cuenta bancaria obligatoria');
            mensajeValidacionFinal += 'Primera cuenta bancaria obligatoria\n';
            ErroresValidacion++;
            return false;
        } else {
        }

        // Se coge las primeras dos letras y se pasan a números
        letra1 = IBAN.substring(0, 1);
        letra2 = IBAN.substring(1, 2);
        num1 = getnumIBAN(letra1);
        num2 = getnumIBAN(letra2);
        //Se sustituye las letras por números.
        isbanaux = String(num1) + String(num2) + IBAN.substring(2);
        // Se mueve los 6 primeros caracteres al final de la cadena.
        isbanaux = isbanaux.substring(6) + isbanaux.substring(0, 6);

        //Se calcula el resto, llamando a la función modulo97, definida más abajo
        resto = modulo97(isbanaux);
        if (resto == 1) {
            //app.alert('el iban es correcto');
            return true;
        } else {
            //app.alert('El iban es incorrecto');
            mensajeValidacionFinal += 'El iban es incorrecto\n';
            ErroresValidacion++;
            return false;
        }
    }

    function modulo97(iban) {
        var parts = Math.ceil(iban.length / 7);
        var remainer = "";

        for (var i = 1; i <= parts; i++) {
            remainer = String(parseFloat(remainer + iban.substr((i - 1) * 7, 7)) % 97);
        }

        return remainer;
    }

    function getnumIBAN(letra) {
        ls_letras = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return ls_letras.search(letra) + 10;
    }

    fn_ValidateIBAN(IBAN);

    //modalidad
    // validaciones para el campo Modalidad
    // comprobacion del campo titulación
    var radioModalidad = this.getField('OPT_MODALIDAD').value;
    if (radioModalidad == "Off") {
        //this.getField('OPT_MODALIDAD').setFocus();
        //app.alert('Debe seleccionar una opción para el campo Modalidad de la colegiación');
        mensajeValidacionFinal += 'Debe seleccionar una opción para el campo Modalidad de la colegiación\n';
        ErroresValidacion++;
    } else {
    }

    //LOPD
    //OJO COMENTADO HASTA SABER QUE FUNCIONALIDAD ES LA ADECUADA
    var chkNoAutorizo = this.getField('CHK_NOAUTORIZO').value;
    var chkAutorizo1 = this.getField('CHK_AUTORIZO1').value;
    var chkAutorizo2 = this.getField('CHK_AUTORIZO2').value;

    /*app.alert('aparejador: '+ titulacionAparejador + ' arquitecto: '+ titulacionArquiectoTecnico + ' grado: ' + titulacionGrado);*/

    if (chkNoAutorizo == "Off" && chkAutorizo1 == "Off" && chkAutorizo2 == "Off") {
        //this.getField('CHK_NOAUTORIZO').setFocus();
        //app.alert('Debe seleccionar las casillas correspondientes a la normativa de protección de datos personales');
    } else { }

    // ACCIONES AL GUARDAR DOCUMENTO O AL PASAR RATON SOBRE FIRMA

    // comprobación del campo estado civil

    var radioEstadoCivil = this.getField('OPT_EC').value;
    if (radioEstadoCivil == "Off") {
        //this.getField('OPT_EC').setFocus();
        //app.alert('Debe seleccionar una opción para el campo Estado Civil');
        mensajeValidacionFinal += 'Debe seleccionar una opción para el campo Estado Civil\n';
        ErroresValidacion++;
    } else {
    }

    // comprobación del campo Sexo

    var radioSexo = this.getField('OPT_S').value;
    if (radioSexo == "Off") {
        //this.getField('OPT_S').setFocus();
        //app.alert('Debe seleccionar una opción para el campo Sexo');
        mensajeValidacionFinal += 'Debe seleccionar una opción para el campo Sexo\n';
        ErroresValidacion++;
    } else {
    }

    // comprobacion del campo titulación
    var titulacionAparejador = this.getField('CHK_TITULO_APAREJADOR').value;
    var titulacionArquitectoTecnico = this.getField('CHK_TITULO_ARQUITECTO').value;
    var titulacionGrado = this.getField('CHK_TITULO_GRADO').value;

    /*app.alert('aparejador: '+ titulacionAparejador + ' arquitecto: '+ titulacionArquiectoTecnico + ' grado: ' + titulacionGrado);*/

    if (titulacionAparejador == "Off" && titulacionArquitectoTecnico == "Off" && titulacionGrado == "Off") {
        //this.getField('CHK_TITULO_APAREJADOR').setFocus();
        //app.alert('Debe seleccionar al menos una opción para el campo Titulación');
        mensajeValidacionFinal += 'Debe seleccionar al menos una opción para el campo Titulación\n';
        ErroresValidacion++;
    } else {

    }

    // comprobacion del campo Conceptos domiciliados en esta cuenta
    var chktodos1 = this.getField('CHK_CD_TODOS_1').value;
    var chkcolegiacion1 = this.getField('CHK_COLEGIACION_1').value;
    var chkvisados1 = this.getField('CHK_CD_VISADOS_1').value;
    var chkcuotaFija1 = this.getField('CHK_CD_SCR_CUOTA_FIJA_1').value;
    var chkcuotaComplementaria1 = this.getField('CHK_CD_CUOTACOMPLEMENTARIA_1').value;
    var chkotrosSeguros1 = this.getField('CHK_CD_OTROS_SEGUROS_1').value;
    var chkpremaat1 = this.getField('CHK_CD_PREMAAT_1').value;
    var chkformacion1 = this.getField('CHK_CD_FORMACIÓN_1').value;
    var chkeventos1 = this.getField('CHK_CD_EVENTOS_1').value;

    /*app.alert('aparejador: '+ titulacionAparejador + ' arquitecto: '+ titulacionArquiectoTecnico + ' grado: ' + titulacionGrado);*/

    if (chktodos1 == "Off" &&
        chkcolegiacion1 == "Off" &&
        chkvisados1 == "Off" &&
        chkcuotaFija1 == "Off" &&
        chkcuotaComplementaria1 == "Off" &&
        chkotrosSeguros1 == "Off" &&
        chkpremaat1 == "Off" &&
        chkformacion1 == "Off" &&
        chkeventos1 == "Off") {
        //this.getField('CHK_CD_TODOS_1').setFocus();
        //app.alert('Debe seleccionar al menos una opción para el campo Conceptos domiciliados en esta cuenta');
        mensajeValidacionFinal += 'Debe seleccionar al menos una opción para el campo Conceptos domiciliados en esta cuenta\n';
        ErroresValidacion++;

    } else {

    }

    // validación para comporobar si esa seleccionada el concepto de la segunda cuota
    var iban2 = this.getField('TXT_COD_IBAN_2').value.toString();
    var banco2 = this.getField('TXT_BANCO_2').value.toString();
    var agencia2 = this.getField('TXT_AGENCIA_2').value.toString();
    var dc2 = this.getField('TXT_DC_2').value.toString();
    var nCuenta2 = this.getField('TXT_NUMCUENTA_2').value.toString();

    if (iban2 != "" || banco2 != "" || agencia2 != "" || dc2 != "") {
        // comprobacion del campo Conceptos domiciliados en esta cuenta
        var chktodos2 = this.getField('CHK_CD_TODOS_2').value;
        var chkcolegiacion2 = this.getField('CHK_COLEGIACION_2').value;
        var chkvisados2 = this.getField('CHK_CD_VISADOS_2').value;
        var chkcuotaFija2 = this.getField('CHK_CD_SCR_CUOTA_FIJA_2').value;
        var chkcuotaComplementaria2 = this.getField('CHK_CD_CUOTACOMPLEMENTARIA_2').value;
        var chkotrosSeguros2 = this.getField('CHK_CD_OTROS_SEGUROS_2').value;
        var chkpremaat2 = this.getField('CHK_CD_PREMAAT_2').value;
        var chkformacion2 = this.getField('CHK_CD_FORMACIÓN_2').value;
        var chkeventos2 = this.getField('CHK_CD_EVENTOS_2').value;

        // Funcionalidad para que no elimine los 0
        while (banco2.length < 4) {
            banco2 = '0' + banco2;
        }
        while (agencia2.length < 4) {
            agencia2 = '0' + agencia2;
        }
        while (dc2.length < 2) {
            dc2 = '0' + dc2;
        }
        while (nCuenta2.length < 10) {
            nCuenta2 = '0' + nCuenta2;
        }

        numeroCuenta2 = banco2 + agencia2 + dc2 + nCuenta2;
        ibanconCuenta2 = iban2 + banco2 + agencia2 + dc2 + nCuenta2; //cuenta con iban
        //app.alert(banco1);
        //app.alert(agencia1);
        //app.alert(dc1);
        //app.alert(nCuenta1);
        //app.alert(numeroCuenta1);
        //app.alert(dc1);
        //app.alert(nCuenta1);

        valores = new Array(1, 2, 4, 8, 5, 10, 9, 7, 3, 6);
        controlCS = 0;
        controlCC = 0;
        for (i = 0; i <= 7; i++)
            controlCS += parseInt(numeroCuenta2.charAt(i)) * valores[i + 2];
        controlCS = 11 - (controlCS % 11);
        if (controlCS == 11) controlCS = 0;
        else if (controlCS == 10) controlCS = 1;

        for (i = 10; i <= 19; i++)
            controlCC += parseInt(numeroCuenta2.charAt(i)) * valores[i - 10];
        controlCC = 11 - (controlCC % 11);
        if (controlCC == 11) controlCC = 0;
        else if (controlCC == 10) controlCC = 1;

        if (numeroCuenta2.charAt(8) == controlCS && numeroCuenta2.charAt(9) == controlCC) {
            //app.alert('numero de cuenta valido');
            //app.alert(numeroCuenta2);

        } else {
            //app.alert('El segundo numero de cuenta es invalido o faltan digitos, reviselo');
            mensajeValidacionFinal += 'El segundo numero de cuenta es invalido o faltan digitos, reviselo\n';
            ErroresValidacion++;

            //app.alert(numeroCuenta2);
        }

        /* Validar el iban de la cuenta bancaria */
        // Funcionalidad para que no elimine los 0
        // Validacion del numero de cuenta 1
        var iban2 = this.getField('TXT_COD_IBAN_2').value.toString();
        var banco2 = this.getField('TXT_BANCO_2').value.toString();
        var agencia2 = this.getField('TXT_AGENCIA_2').value.toString();
        var dc2 = this.getField('TXT_DC_2').value.toString();
        var nCuenta2 = this.getField('TXT_NUMCUENTA_2').value.toString();

        // Funcionalidad para que no elimine los 0 si el valor inicia por este
        while (banco2.length < 4) {
            banco2 = '0' + banco2;
        }
        while (agencia2.length < 4) {
            agencia2 = '0' + agencia2;
        }
        while (dc2.length < 2) {
            dc2 = '0' + dc2;
        }
        while (nCuenta2.length < 10) {
            nCuenta2 = '0' + nCuenta2;
        }

        numeroCuenta2 = banco2 + agencia2 + dc2 + nCuenta2;
        IBAN2 = iban2 + banco2 + agencia2 + dc2 + nCuenta2; //cuenta con iban

        function fn_ValidateIBAN(IBAN2) {

            //Se pasa a Mayusculas
            IBAN2 = IBAN2.toUpperCase();
            //Se quita los blancos de principio y final.
            //IBAN = IBAN.trim();
            IBAN2 = IBAN2.replace(/\s/g, ""); //Y se quita los espacios en blanco dentro de la cadena

            var letra1, letra2, num1, num2;
            var isbanaux;
            var numeroSustitucion;
            //La longitud debe ser siempre de 24 caracteres
            if (IBAN2.length != 24) {
                //app.alert('La segunda cuenta bancaria debe de tener 24 digitos');
                mensajeValidacionFinal += 'La segunda cuenta bancaria debe de tener 24 digitos\n';
                ErroresValidacion++;
                return false;
            } else {

            }

            // Se coge las primeras dos letras y se pasan a números
            letra1 = IBAN2.substring(0, 1);
            letra2 = IBAN2.substring(1, 2);
            num1 = getnumIBAN(letra1);
            num2 = getnumIBAN(letra2);
            //Se sustituye las letras por números.
            isbanaux = String(num1) + String(num2) + IBAN2.substring(2);
            // Se mueve los 6 primeros caracteres al final de la cadena.
            isbanaux = isbanaux.substring(6) + isbanaux.substring(0, 6);

            //Se calcula el resto, llamando a la función modulo97, definida más abajo
            resto = modulo97(isbanaux);
            if (resto == 1) {
                //app.alert('el iban es correcto en la cuenta 2');

                return true;
            } else {
                //app.alert('El iban es incorrecto');
                mensajeValidacionFinal += 'El iban es incorrecto';
                ErroresValidacion++;
                return false;
            }
        }

        function modulo97(iban) {
            var parts = Math.ceil(iban.length / 7);
            var remainer = "";

            for (var i = 1; i <= parts; i++) {
                remainer = String(parseFloat(remainer + iban.substr((i - 1) * 7, 7)) % 97);
            }

            return remainer;
        }

        function getnumIBAN(letra) {
            ls_letras = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            return ls_letras.search(letra) + 10;
        }

        fn_ValidateIBAN(IBAN2);

        if (chktodos2 == "Off" &&
            chkcolegiacion2 == "Off" &&
            chkvisados2 == "Off" &&
            chkcuotaFija2 == "Off" &&
            chkcuotaComplementaria2 == "Off" &&
            chkotrosSeguros2 == "Off" &&
            chkpremaat2 == "Off" &&
            chkformacion2 == "Off" &&
            chkeventos2 == "Off") {
            //this.getField('CHK_CD_TODOS_2').setFocus();
            //app.alert('Debe seleccionar al menos una opción para el campo Conceptos domiciliados en la segunda cuenta');
            mensajeValidacionFinal += 'Debe seleccionar al menos una opción para el campo Conceptos domiciliados en la segunda cuenta\n';
            ErroresValidacion++;

        } else {

        }
    } else {

    }

    // validaciones para el campo Modalidad
    // comprobacion del campo titulación
    var radioModalidad = this.getField('OPT_MODALIDAD').value;
    if (radioModalidad == "Off") {
        //this.getField('OPT_MODALIDAD').setFocus();
        //app.alert('Debe seleccionar una opción para el campo Modalidad de la colegiación');
        mensajeValidacionFinal += 'Debe seleccionar una opción para el campo Modalidad de la colegiación\n';
        ErroresValidacion++;
    } else {

    }

    // comprobacion del campo LOPD
    var chkNoAutorizo = this.getField('CHK_NOAUTORIZO').value;
    var chkAutorizo1 = this.getField('CHK_AUTORIZO1').value;
    var chkAutorizo2 = this.getField('CHK_AUTORIZO2').value;

    /*app.alert('aparejador: '+ titulacionAparejador + ' arquitecto: '+ titulacionArquiectoTecnico + ' grado: ' + titulacionGrado);*/

    if (chkNoAutorizo == "Off" && chkAutorizo1 == "Off" && chkAutorizo2 == "Off") {
        //this.getField('CHK_NOAUTORIZO').setFocus();
        //app.alert('Debe seleccionar las casillas correspondientes a la normativa de protección de datos personales');

    } else {

    }

    if (ErroresValidacion == 0) {
        firmaDigital.hidden = false;
        firmaInput.hidden = true;
    } else {
        app.alert(mensajeValidacionFinal);
        firmaDigital.hidden = true;
        firmaInput.hidden = false;
    }

} // final Validar()
