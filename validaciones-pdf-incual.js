/********************** valores automaticos al rellenar campos iniciales ***********************/

// NUMERO DE REGISTRO
var numeroRegistro = this.getField('numero-registro-1').value;

if (numeroRegistro == "" || numeroRegistro == null) {
    //mensajeValidacionFinal += 'Por favor introduce un valor en el campo NÚMERO DE REGISTRO\n';
    //ErroresValidacion++;
    this.getField('numero-registro-2-lectura').value = numeroRegistro;
    this.getField('numero-registro-3-lectura').value = numeroRegistro;
    //app.alert('campo vacio');
} else {
    this.getField('numero-registro-2-lectura').value = numeroRegistro;
    this.getField('numero-registro-3-lectura').value = numeroRegistro;
    //app.alert('campo relleno');
}

// FECHA INICIO ALERTA
var fechaInicioAlerta = this.getField('fecha-inicio-alerta-1').value;

if (fechaInicioAlerta == "" || fechaInicioAlerta == null) {
    //mensajeValidacionFinal += 'Por favor introduce un valor en el campo FECHA INICIO ALERTA\n';
    //ErroresValidacion++;
    this.getField('fecha-inicio-alerta-2-lectura').value = fechaInicioAlerta;
    this.getField('fecha-inicio-alerta-3-lectura').value = fechaInicioAlerta;
    //app.alert('campo vacio');
} else {
    this.getField('fecha-inicio-alerta-2-lectura').value = fechaInicioAlerta;
    this.getField('fecha-inicio-alerta-3-lectura').value = fechaInicioAlerta;
    //app.alert('campo relleno');
}

// FECHA DE ACTUALIZACIÓN ALERTA
var fechaActualizacionAlerta = this.getField('fecha-actualizacion-alerta-1').value;

if (fechaActualizacionAlerta == "" || fechaActualizacionAlerta == null) {
    //mensajeValidacionFinal += 'Por favor introduce un valor en el campo FECHA DE ACTUALIZACIÓN ALERTA\n';
    //ErroresValidacion++;
    this.getField('fecha-actualizacion-alerta-2-lectura').value = fechaActualizacionAlerta;
    this.getField('fecha-actualizacion-alerta-3-lectura').value = fechaActualizacionAlerta;
    //app.alert('campo vacio');
} else {
    this.getField('fecha-actualizacion-alerta-2-lectura').value = fechaActualizacionAlerta;
    this.getField('fecha-actualizacion-alerta-3-lectura').value = fechaActualizacionAlerta;
    //app.alert('campo relleno');
}

// ENTIDAD QUE REGISTRA LA ALERTA
var entidadRegistraAlerta = this.getField('entidad-registra-alerta-1').value;

if (entidadRegistraAlerta == "" || entidadRegistraAlerta == null) {
    //mensajeValidacionFinal += 'Por favor introduce un valor en el campo ENTIDAD QUE REGISTRA LA ALERTA\n';
    //ErroresValidacion++;
    this.getField('entidad-registra-alerta-2-lectura').value = entidadRegistraAlerta;
    this.getField('entidad-registra-alerta-3-lectura').value = entidadRegistraAlerta;
    //app.alert('campo vacio');
} else {
    this.getField('entidad-registra-alerta-2-lectura').value = entidadRegistraAlerta;
    this.getField('entidad-registra-alerta-3-lectura').value = entidadRegistraAlerta;
    //app.alert('campo relleno');
}

// CODIGO
var codigo = this.getField('codigo-1').value;

if (codigo == "" || codigo == null) {
    //mensajeValidacionFinal += 'Por favor introduce un valor en el campo CÓDIGO\n';
    //ErroresValidacion++;
    this.getField('codigo-2-lectura').value = codigo;
    this.getField('codigo-3-lectura').value = codigo;
    //app.alert('campo vacio');
} else {
    this.getField('codigo-2-lectura').value = codigo;
    this.getField('codigo-3-lectura').value = codigo;
    //app.alert('campo relleno');
}

// FAMILIA PROFESIONAL
var familiaProfesional = this.getField('familia-profesional-1').value;

if (familiaProfesional == " " || familiaProfesional == "0") {
    //mensajeValidacionFinal += 'Por favor introduce un valor en el campo FAMILIA PROFESIONAL\n';
    //ErroresValidacion++;
    this.getField('familia-profesional-2-lectura').value = familiaProfesional;
    this.getField('familia-profesional-3-lectura').value = familiaProfesional;
    //app.alert('campo vacio');
} else {
    this.getField('familia-profesional-2-lectura').value = familiaProfesional;
    this.getField('familia-profesional-3-lectura').value = familiaProfesional;
    //app.alert('campo relleno');
}

// DENOMINACIÓN EXISTENTE O PROPUESTA
var denominacionExistentePropuesta = this.getField('denominacion-existente-o-propuesta-1').value;

if (denominacionExistentePropuesta == "" || denominacionExistentePropuesta == null) {
    //mensajeValidacionFinal += 'Por favor introduce un valor en el campo DENOMINACIÓN EXISTENTE O PROPUESTA\n';
    //ErroresValidacion++;
    this.getField('denominacion-existente-o-propuesta-2-lectura').value = denominacionExistentePropuesta;
    this.getField('denominacion-existente-o-propuesta-3-lectura').value = denominacionExistentePropuesta;
    //app.alert('campo vacio');
} else {
    this.getField('denominacion-existente-o-propuesta-2-lectura').value = denominacionExistentePropuesta;
    this.getField('denominacion-existente-o-propuesta-3-lectura').value = denominacionExistentePropuesta;
    //app.alert('campo relleno');
}


/********************** Validaciones generales ***********************/

function ValidarFormulario() {
    // VARIABLES GLOBALES

    ErroresValidacion = 0;
    mensajeValidacionFinal = "";
    var expresion_regular_fecha = /^([0][1-9]|[12][0-9]|3[01])(\/|-)([0][1-9]|[1][0-2])\2(\d{4})$/;
    var expresion_regular_correo = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
    
    /////////////////////////////////

    // NUMERO DE REGISTRO
    var numeroRegistro = this.getField('numero-registro-1').value;

    if (numeroRegistro == "" || numeroRegistro == null) {
        mensajeValidacionFinal += 'Por favor introduce un valor en el campo NÚMERO DE REGISTRO\n';
        ErroresValidacion++;
        this.getField('numero-registro-2-lectura').value = numeroRegistro;
        this.getField('numero-registro-3-lectura').value = numeroRegistro;
        //app.alert('campo vacio');
    } else {
        this.getField('numero-registro-2-lectura').value = numeroRegistro;
        this.getField('numero-registro-3-lectura').value = numeroRegistro;
        //app.alert('campo relleno');
    }

    // FECHA INICIO ALERTA
    var fechaInicioAlerta = this.getField('fecha-inicio-alerta-1').value;

    if ((expresion_regular_fecha.test(fechaInicioAlerta) == true) && (fechaInicioAlerta != '')) {
        // es una fecha valida
        this.getField('fecha-inicio-alerta-2-lectura').value = fechaInicioAlerta;
        this.getField('fecha-inicio-alerta-3-lectura').value = fechaInicioAlerta;
    } else {
        this.getField('fecha-inicio-alerta-2-lectura').value = fechaInicioAlerta;
        this.getField('fecha-inicio-alerta-3-lectura').value = fechaInicioAlerta;
        mensajeValidacionFinal += 'El campo FECHA INICIO ALERTA esta vacio o no tiene el formato correcto.\n';
        ErroresValidacion++;
    }

    // FECHA DE ACTUALIZACIÓN ALERTA
    var fechaActualizacionAlerta = this.getField('fecha-actualizacion-alerta-1').value;

    if ((expresion_regular_fecha.test(fechaActualizacionAlerta) == true) && (fechaActualizacionAlerta != '')) {
        // es una fecha valida
        this.getField('fecha-actualizacion-alerta-2-lectura').value = fechaActualizacionAlerta;
        this.getField('fecha-actualizacion-alerta-3-lectura').value = fechaActualizacionAlerta;
    } else {
        this.getField('fecha-actualizacion-alerta-2-lectura').value = fechaActualizacionAlerta;
        this.getField('fecha-actualizacion-alerta-3-lectura').value = fechaActualizacionAlerta;
        mensajeValidacionFinal += 'El campo FECHA DE ACTUALIZACIÓN ALERTA esta vacio o no tiene el formato correcto.\n';
        ErroresValidacion++;
    }

    // PERSONA DE CONTACTO
    var personaContacto = this.getField('persona-contacto-1').value;

    if (personaContacto == "" || personaContacto == null) {
        mensajeValidacionFinal += 'Por favor introduce un valor en el campo PERSONA DE CONTACTO\n';
        ErroresValidacion++;
        //app.alert('campo vacio');
    } else {
        //app.alert('campo relleno');
    }

    // DIRECCIÓN
    var direccion = this.getField('direccion-1').value;

    if (direccion == "" || direccion == null) {
        mensajeValidacionFinal += 'Por favor introduce un valor en el campo DIRECCIÓN\n';
        ErroresValidacion++;
        //app.alert('campo vacio');
    } else {
        //app.alert('campo relleno');
    }

    // TELEFONO 1
    var telefonoUno = this.getField('telefono-1').value;

    if (telefonoUno == "" || telefonoUno == null) {
        mensajeValidacionFinal += 'Por favor introduce un valor en el campo TELÉFONO 1\n';
        ErroresValidacion++;
        //app.alert('campo vacio');
    } else {
        //app.alert('campo relleno');
    }

    // TELEFONO 2
    /*var telefonoDos = this.getField('telefono-2').value;

    if (telefonoDos == "" || telefonoDos == null) {
        mensajeValidacionFinal += 'Por favor introduce un valor en el campo TELÉFONO 2\n';
        ErroresValidacion++;
        //app.alert('campo vacio');
    } else {
        //app.alert('campo relleno');
    }*/

    // E-MAIL
    var email = this.getField('e-mail-1').value;

    if ((expresion_regular_correo.test(email) == true) && (email != '')) {
        // es un correo valido
    } else {
        //app.alert('El campo email tiene un formato incorrecto.');
        mensajeValidacionFinal += 'El campo E-MAIL tiene un formato incorrecto.\n';
    }

    // ENTIDAD QUE REGISTRA LA ALERTA
    var entidadRegistraAlerta = this.getField('entidad-registra-alerta-1').value;

    if (entidadRegistraAlerta == "" || entidadRegistraAlerta == null) {
        mensajeValidacionFinal += 'Por favor introduce un valor en el campo ENTIDAD QUE REGISTRA LA ALERTA\n';
        ErroresValidacion++;
        this.getField('entidad-registra-alerta-2-lectura').value = entidadRegistraAlerta;
        this.getField('entidad-registra-alerta-3-lectura').value = entidadRegistraAlerta;
        //app.alert('campo vacio');
    } else {
        this.getField('entidad-registra-alerta-2-lectura').value = entidadRegistraAlerta;
        this.getField('entidad-registra-alerta-3-lectura').value = entidadRegistraAlerta;
        //app.alert('campo relleno');
    }

    // LA ALERTA SE PRODUCE SOBRE
    var radioAlertaProduceSobre = this.getField('alerta-produce-1').value;
    if (radioAlertaProduceSobre == "Off") {
        //this.getField('OPT_MODALIDAD').setFocus();
        //app.alert('Debe seleccionar una opción para el campo Modalidad de la colegiación');
        mensajeValidacionFinal += 'Debe seleccionar una opción para el campo LA ALERTA SE PRODUCE SOBRE\n';
        ErroresValidacion++;
        //app.alert(radioAlertaProduceSobre);
    }else if(radioAlertaProduceSobre == "cualificacion-nueva") {
        this.getField('alerta-produce-2').value = radioAlertaProduceSobre;
        this.getField('alerta-produce-3').value = radioAlertaProduceSobre;
    }else if(radioAlertaProduceSobre == "cualificacion-existente") {
        this.getField('alerta-produce-2').value = radioAlertaProduceSobre;
        this.getField('alerta-produce-3').value = radioAlertaProduceSobre;
    }
    else {
        //app.alert(radioAlertaProduceSobre);
    }

    // NIVEL DE LA CUALIFICACIÓN EXISTENTE O PROPUESTO
    var radioNivelCualificacion = this.getField('nivel-cualificacion-existente-o-propuesto-1').value;
    if (radioNivelCualificacion == "Off") {
        //this.getField('OPT_MODALIDAD').setFocus();
        //app.alert('Debe seleccionar una opción para el campo Modalidad de la colegiación');
        mensajeValidacionFinal += 'Debe seleccionar una opción para el campo NIVEL DE LA CUALIFICACIÓN EXISTENTE O PROPUESTO\n';
        ErroresValidacion++;
        //app.alert(radioNivelCualificacion);
    }
    else if(radioNivelCualificacion == "nivel-1") {
        this.getField('nivel-cualificacion-existente-o-propuesto-2-lectura').value = radioNivelCualificacion;
        this.getField('nivel-cualificacion-existente-o-propuesto-3-lectura').value = radioNivelCualificacion;
    }else if(radioNivelCualificacion == "nivel-2") {
        this.getField('nivel-cualificacion-existente-o-propuesto-2-lectura').value = radioNivelCualificacion;
        this.getField('nivel-cualificacion-existente-o-propuesto-3-lectura').value = radioNivelCualificacion;
    }
    else if(radioNivelCualificacion == "nivel-3") {
        this.getField('nivel-cualificacion-existente-o-propuesto-2-lectura').value = radioNivelCualificacion;
        this.getField('nivel-cualificacion-existente-o-propuesto-3-lectura').value = radioNivelCualificacion;
    }
    else {
        //app.alert(radioNivelCualificacion);
    }

    // TIPO DE ORGANIZACIÓN

    var chkTipoEmpresa = this.getField('tipo-organizacion-empresa-1').value;
    var chkTipoAsoEmpresarial = this.getField('tipo-organizacion-asociacion-empresarial-1').value;
    var chkTipoRepresentacion = this.getField('tipo-organizacion-representacion-sindical-colegial-1').value;
    var chkTipoAdmGeneral = this.getField('tipo-organizacion-administracion-general-1').value;
    var chkTipoAdmAutononima = this.getField('tipo-organizacion-administracion-autonomica-1').value;
    var chkTipoAsoProfesional = this.getField('tipo-organizacion-asociacion-profesional-1').value;
    var chkTipoOtra = this.getField('tipo-organizacion-otra-1').value;

    if (chkTipoEmpresa == "Off" &&
        chkTipoAsoEmpresarial == "Off" &&
        chkTipoRepresentacion == "Off" &&
        chkTipoAdmGeneral == "Off" &&
        chkTipoAdmAutononima == "Off" &&
        chkTipoAsoProfesional == "Off" &&
        chkTipoOtra == "Off") {
        //this.getField('CHK_CD_TODOS_1').setFocus();
        //app.alert('Debe seleccionar al menos una opción para el campo Conceptos domiciliados en esta cuenta');
        mensajeValidacionFinal += 'Debe seleccionar al menos una opción para el campo TIPO DE ORGANIZACIÓN\n';
        ErroresValidacion++;

    } else {

    }

    // CODIGO
    var codigo = this.getField('codigo-1').value;

    if (codigo == "" || codigo == null) {
        mensajeValidacionFinal += 'Por favor introduce un valor en el campo CÓDIGO\n';
        ErroresValidacion++;
        this.getField('codigo-2-lectura').value = codigo;
        this.getField('codigo-3-lectura').value = codigo;
        //app.alert('campo vacio');
    } else {
        this.getField('codigo-2-lectura').value = codigo;
        this.getField('codigo-3-lectura').value = codigo;
        //app.alert('campo relleno');
    }

    // FAMILIA PROFESIONAL
    var familiaProfesional = this.getField('familia-profesional-1').value;

    if (familiaProfesional == " " || familiaProfesional == "0") {
        mensajeValidacionFinal += 'Por favor introduce un valor en el campo FAMILIA PROFESIONAL\n';
        ErroresValidacion++;
        this.getField('familia-profesional-2-lectura').value = familiaProfesional;
        this.getField('familia-profesional-3-lectura').value = familiaProfesional;
        //app.alert('campo vacio');
    } else {
        this.getField('familia-profesional-2-lectura').value = familiaProfesional;
        this.getField('familia-profesional-3-lectura').value = familiaProfesional;
        //app.alert('campo relleno');
    }

    // DENOMINACIÓN EXISTENTE O PROPUESTA
    var denominacionExistentePropuesta = this.getField('denominacion-existente-o-propuesta-1').value;

    if (denominacionExistentePropuesta == "" || denominacionExistentePropuesta == null) {
        mensajeValidacionFinal += 'Por favor introduce un valor en el campo DENOMINACIÓN EXISTENTE O PROPUESTA\n';
        ErroresValidacion++;
        this.getField('denominacion-existente-o-propuesta-2-lectura').value = denominacionExistentePropuesta;
        this.getField('denominacion-existente-o-propuesta-3-lectura').value = denominacionExistentePropuesta;
        //app.alert('campo vacio');
    } else {
        this.getField('denominacion-existente-o-propuesta-2-lectura').value = denominacionExistentePropuesta;
        this.getField('denominacion-existente-o-propuesta-3-lectura').value = denominacionExistentePropuesta;
        //app.alert('campo relleno');
    }

    // BREVE DESCRIPCIÓN DEL MOTIVO O MOTIVOS DE LA ALERTA
    var breveDescripcionMotivo = this.getField('breve-descripcion-motivo-alerta-1').value;

    if (breveDescripcionMotivo == "" || breveDescripcionMotivo == null) {
        mensajeValidacionFinal += 'Por favor introduce un valor en el campo BREVE DESCRIPCIÓN DEL MOTIVO O MOTIVOS DE LA ALERTA\n';
        ErroresValidacion++;
        //app.alert('campo vacio');
    } else {
        //app.alert('campo relleno');
    }

    /* PAGINA 2 */

    // TIPIFICACION DE LA ALERTA

    var chkTipificacionAlertaCambiosActuales = this.getField('tipificacion-alerta-cambios-actuales-procesos-2').value;
    var chkTipificacionAlertaCambiosFuturo = this.getField('tipificacion-alerta-cambios-futuro-procesos-2').value;
    var chkTipificacionAlertaNuevaLegislacion = this.getField('tipificacion-alerta-nueva-legislacion-2').value;
    var chkTipificacionAlertaCambiosRegulacion = this.getField('tipificacion-alerta-cambios-regulacion-2').value;
    var chkTipificacionAlertaDesaparicionPerfiles = this.getField('tipificacion-alerta-desaparicion-perfiles-2').value;
    var chkTipificacionAlertaInclusionPerfiles = this.getField('tipificacion-alerta-inclusion-perfiles-2').value;
    var chkTipificacionAlertaOtros = this.getField('tipificacion-alerta-otros-2').value;

    if (chkTipificacionAlertaCambiosActuales == "Off" &&
        chkTipificacionAlertaCambiosFuturo == "Off" &&
        chkTipificacionAlertaNuevaLegislacion == "Off" &&
        chkTipificacionAlertaCambiosRegulacion == "Off" &&
        chkTipificacionAlertaDesaparicionPerfiles == "Off" &&
        chkTipificacionAlertaInclusionPerfiles == "Off" &&
        chkTipificacionAlertaOtros == "Off") {
        //this.getField('CHK_CD_TODOS_1').setFocus();
        //app.alert('Debe seleccionar al menos una opción para el campo Conceptos domiciliados en esta cuenta');
        mensajeValidacionFinal += 'Debe seleccionar al menos una opción para el campo TIPIFICACION DE LA ALERTA\n';
        ErroresValidacion++;

    } else {

    }

    // SI MARCA OTROS, ESPECIFICAR
    var marcaOtros = this.getField('marca-otros-2').value;

    if((chkTipificacionAlertaOtros == "Sí") && (marcaOtros == '')) {
        mensajeValidacionFinal += 'Por favor introduce un valor en el campo SI MARCA OTROS, ESPECIFICAR\n';
        ErroresValidacion++;
    }else{

    }

    // SECTORES DE ACTIVIDAD

    var chkSectoresActividadPrimario = this.getField('sectores-actividad-primario-2').value;
    var chkSectoresActividadIndustria = this.getField('sectores-actividad-industria-2').value;
    var chkSectoresActividadServicios = this.getField('sectores-actividad-servicios-2').value;
    var chkSectoresActividadTercer = this.getField('sectores-actividad-tercer-2').value;
    var chkSectoresActividadCuarto = this.getField('sectores-actividad-cuarto-2').value;
    var chkSectoresActividadTransversal = this.getField('sectores-actividad-transversal-2').value;
    var chkSectoresActividadEconomia = this.getField('sectores-actividad-economia-2').value;

    if (chkSectoresActividadPrimario == "Off" &&
        chkSectoresActividadIndustria == "Off" &&
        chkSectoresActividadServicios == "Off" &&
        chkSectoresActividadTercer == "Off" &&
        chkSectoresActividadCuarto == "Off" &&
        chkSectoresActividadTransversal == "Off" &&
        chkSectoresActividadEconomia == "Off") {
        //this.getField('CHK_CD_TODOS_1').setFocus();
        //app.alert('Debe seleccionar al menos una opción para el campo Conceptos domiciliados en esta cuenta');
        mensajeValidacionFinal += 'Debe seleccionar al menos una opción para el campo SECTORES ACTIVIDAD\n';
        ErroresValidacion++;
    } else {

    }

    // PROPIEDAD DEL CAPITAL
    var radioPropiedadCapital = this.getField('propiedad-capital-2').value;
    if (radioPropiedadCapital == "Off") {
        //this.getField('propiedad-capital-2').setFocus();
        mensajeValidacionFinal += 'Debe seleccionar una opción para el campo PROPIEDAD DEL CAPITAL\n';
        ErroresValidacion++;
        //app.alert(radioPropiedadCapital);
    }
    else {
        //app.alert(radioPropiedadCapital);
    }

    // TAMAÑO EMPRESARIAL

    var chkTamanoEmpresarialGrande = this.getField('tamano-empresarial-grande-2').value;
    var chkTamanoEmpresarialMediano = this.getField('tamano-empresarial-mediano-2').value;
    var chkTamanoEmpresarialPequeno = this.getField('tamano-empresarial-pequeno-2').value;
    var chkTamanoEmpresarialMicro = this.getField('tamano-empresarial-micro-2').value;
    var chkTamanoEmpresarialAutonomo = this.getField('tamano-empresarial-autonomo-2').value;

    if (chkTamanoEmpresarialGrande == "Off" &&
        chkTamanoEmpresarialMediano == "Off" &&
        chkTamanoEmpresarialPequeno == "Off" &&
        chkTamanoEmpresarialMicro == "Off" &&
        chkTamanoEmpresarialAutonomo == "Off") {
        //this.getField('CHK_CD_TODOS_1').setFocus();
        //app.alert('Debe seleccionar al menos una opción para el campo Conceptos domiciliados en esta cuenta');
        mensajeValidacionFinal += 'Debe seleccionar al menos una opción para el campo SECTORES ACTIVIDAD\n';
        ErroresValidacion++;
    } else {

    }

    // ÁMBITO GEOGRÁFICO DE ACTIVIDAD

    var chkAmbitoGeograficoLocal = this.getField('ambito-geografico-local-2').value;
    var chkAmbitoGeograficoProvincial = this.getField('ambito-geografico-provincial-2').value;
    var chkAmbitoGeograficoRegional = this.getField('ambito-geografico-regional-2').value;
    var chkAmbitoGeograficoNacional = this.getField('ambito-geografico-nacional-2').value;
    var chkAmbitoGeograficoEuropeo = this.getField('ambito-geografico-europeo-2').value;
    var chkAmbitoGeograficoInternacional = this.getField('ambito-geografico-internacional-2').value;

    if (chkAmbitoGeograficoLocal == "Off" &&
        chkAmbitoGeograficoProvincial == "Off" &&
        chkAmbitoGeograficoRegional == "Off" &&
        chkAmbitoGeograficoNacional == "Off" &&
        chkAmbitoGeograficoEuropeo == "Off" &&
        chkAmbitoGeograficoInternacional == "Off") {
        //this.getField('CHK_CD_TODOS_1').setFocus();
        //app.alert('Debe seleccionar al menos una opción para el campo Conceptos domiciliados en esta cuenta');
        mensajeValidacionFinal += 'Debe seleccionar al menos una opción para el campo ÁMBITO GEOGRÁFICO DE ACTIVIDAD\n';
        ErroresValidacion++;
    } else {

    }

    // REGULACIÓN PROFESIONAL

    var chkRegProfesionalProfesion = this.getField('regulacion-profesion-2').value;
    var chkRegProfesionalActividad = this.getField('regulacion-actividad-2').value;
    var chkRegProfesionalProfesionNo = this.getField('regulacion-profesion-no-2').value;

    if (chkRegProfesionalProfesion == "Off" &&
        chkRegProfesionalActividad == "Off" &&
        chkRegProfesionalProfesionNo == "Off") {
        //this.getField('CHK_CD_TODOS_1').setFocus();
        //app.alert('Debe seleccionar al menos una opción para el campo Conceptos domiciliados en esta cuenta');
        mensajeValidacionFinal += 'Debe seleccionar al menos una opción para el campo REGULACIÓN PROFESIONAL\n';
        ErroresValidacion++;
    } else {

    }

    // DIRECTIVA/REGLAMENTO EUROPEO

    var chkDRDirectiva = this.getField('directiva-UE-2').value;
    var chkDRReglamento = this.getField('reglamento-UE-2').value;
    var chkDRRecomendacion = this.getField('recomendacion-UE-2').value;
    var chkDROtros = this.getField('otros-UE-2').value;

    if (chkDRDirectiva == "Off" &&
        chkDRReglamento == "Off" &&
        chkDRRecomendacion == "Off" &&
        chkDROtros) {
        //this.getField('CHK_CD_TODOS_1').setFocus();
        //app.alert('Debe seleccionar al menos una opción para el campo Conceptos domiciliados en esta cuenta');
        mensajeValidacionFinal += 'Debe seleccionar al menos una opción para el campo DIRECTIVA/REGLAMENTO EUROPEO\n';
        ErroresValidacion++;
    } else {

    }

    // ADMINISTRACIONES INTERESADAS
    var administracionesInteresadas = this.getField('administraciones-interesadas-2').value;

    if (administracionesInteresadas == "" || administracionesInteresadas == null) {
        mensajeValidacionFinal += 'Por favor introduce un valor en el campo ADMINISTRACIONES INTERESADAS\n';
        ErroresValidacion++;
        //app.alert('campo vacio');
    } else {
        //app.alert('campo relleno');
    }

    /* PAGINA 3 */

    // PROPUESTAS

    var chkPropuestasCrearNueva = this.getField('propuestas-crear-nueva-3').value;
    var chkPropuestasCambiarNivel = this.getField('propuestas-cambiar-nivel-3').value;
    var chkPropuestasCambiarDenominacion = this.getField('propuestas-cambiar-denominacion-3').value;
    var chkPropuestasSuprimirCualificacion = this.getField('propuestas-suprimir-cualificacion-3').value;
    var chkPropuestasCrearUCS = this.getField('propuestas-crear-ucs-3').value;
    var chkPropuestasModificarUCS = this.getField('propuestas-modificar-ucs-3').value;
    var chkPropuestasSuprimirUCS = this.getField('propuestas-suprimir-ucs-3').value;
    var chkPropuestasModificarParametros = this.getField('propuestas-modificar-parametros-3').value;
    var chkPropuestasModificarModulos = this.getField('propuestas-modificar-modulos-3').value;
    var chkPropuestasOtrasPropuestas = this.getField('propuestas-otras-propuestas-3').value;

    if (chkPropuestasCrearNueva == "Off" &&
        chkPropuestasCambiarNivel == "Off" &&
        chkPropuestasCambiarDenominacion == "Off" &&
        chkPropuestasSuprimirCualificacion == "Off" &&
        chkPropuestasCrearUCS == "Off" &&
        chkPropuestasModificarUCS == "Off" &&
        chkPropuestasSuprimirUCS == "Off" &&
        chkPropuestasModificarParametros == "Off" &&
        chkPropuestasModificarModulos == "Off" &&
        chkPropuestasOtrasPropuestas) {
        //this.getField('CHK_CD_TODOS_1').setFocus();
        //app.alert('Debe seleccionar al menos una opción para el campo Conceptos domiciliados en esta cuenta');
        mensajeValidacionFinal += 'Debe seleccionar al menos una opción para el campo PROPUESTAS\n';
        ErroresValidacion++;
    } else {

    }

    // PROPUESTAS DETALLADAS
    var propuestasDetalladas = this.getField('propuestas-detalladas-3').value;

    if (propuestasDetalladas == "" || propuestasDetalladas == null) {
        mensajeValidacionFinal += 'Por favor introduce un valor en el campo PROPUESTAS DETALLADAS\n';
        ErroresValidacion++;
        //app.alert('campo vacio');
    } else {
        //app.alert('campo relleno');
    }

    /////////////////////////////////

    // COMPROBACIÓN FINAL VALIDACIÓN

    if (ErroresValidacion == 0) {
        // Todo ok
    } else {
        // ko validación
        app.alert(mensajeValidacionFinal);
    }

}

/********************** Javascript al iniciar el documento ***********************/

function Inicio() {
    var f = new Date();

    var dd;

    if(f.getDate() < 10) {
        var dd = "0";
    }else{
        var dd = "";
    }

    var fechaSistema = dd + f.getDate() + "/" + ("0" + (f.getMonth() +1)).slice(-2) + "/" + f.getFullYear();
    this.getField('fecha-inicio-alerta-1').value = fechaSistema;
    this.getField('fecha-inicio-alerta-2-lectura').value = fechaSistema;
    this.getField('fecha-inicio-alerta-3-lectura').value = fechaSistema;
}
Inicio();